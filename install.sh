#!/bin/bash



### define colors ###
lightred=$'\033[1;31m'  # light red
red=$'\033[0;31m'  # red
lightgreen=$'\033[1;32m'  # light green
green=$'\033[0;32m'  # green
lightblue=$'\033[1;34m'  # light blue
blue=$'\033[0;34m'  # blue
white=$'\033[1;37m'  # white
yellow=$'\033[1;33m'  # yellow
nocolor=$'\e[0m' # no color

echo -e -n "${lightred}"
echo -e -n "${red}"
echo -e -n "${lightgreen}"
echo -e -n "${green}"
echo -e -n "${lightblue}"
echo -e -n "${blue}"
echo -e -n "${white}"
echo -e -n "${yellow}"
echo -e -n "${nocolor}"
clear

# script must be run as root
if [[ $(id -u) -ne 0 ]] ; then printf "\n${lightred} Please run as root${nocolor}\n\n" ; exit 1 ; fi

# Set Vars
LOGFILE='/var/log/server_hardening.log'

function create_swap() {
	# Check for and create swap file if necessary
	# this is an alternative that will disable swap, and create a new one at the size you like
	# sudo swapoff -a && sudo dd if=/dev/zero of=/swapfile bs=1M count=6144 MB && sudo mkswap /swapfile && sudo swapon /swapfile

	# Check for swap file - if none, create one
	if free | awk '/^Swap:/ {exit !$2}'; then
		echo -e -n "${lightred}"
		echo -e "---------------------------------------------------- " | tee -a "$LOGFILE"
		echo -e " $(date +%m.%d.%Y_%H:%M:%S) : Swap exists- No changes made " | tee -a "$LOGFILE"
		echo -e "---------------------------------------------------- \n"  | tee -a "$LOGFILE"
		# sleep 2
		echo -e -n "${nocolor}"
	else
		# set swap to twice the physical RAM but not less than 2GB
		PHYSRAM=$(grep MemTotal /proc/meminfo | awk '{print int($2 / 1024 / 1024 + 0.5)}')
		let "SWAPSIZE=2*$PHYSRAM"
		(($SWAPSIZE >= 1 && $SWAPSIZE >= 31)) && SWAPSIZE=31
		(($SWAPSIZE <= 2)) && SWAPSIZE=2

		fallocate -l ${SWAPSIZE}G /swapfile && chmod 600 /swapfile && mkswap /swapfile && swapon /swapfile && cp /etc/fstab /etc/fstab.bak && echo '/swapfile none swap sw 0 0' | tee -a /etc/fstab
		echo -e -n "${lightgreen}"
		echo -e "-------------------------------------------------- " | tee -a "$LOGFILE"
		echo -e " $(date +%m.%d.%Y_%H:%M:%S) : SWAP CREATED SUCCESSFULLY " | tee -a "$LOGFILE"
		echo -e "-------------------------------------------------- \n" | tee -a "$LOGFILE"
		# sleep 2
		echo -e -n "${nocolor}"
	fi
}


function update_system() {
	echo -e -n "${yellow}"
	echo -e "---------------------------------------------------- " | tee -a "$LOGFILE"
	echo -e " $(date +%m.%d.%Y_%H:%M:%S) : UPDATING SYSTEM " | tee -a "$LOGFILE"
	echo -e "---------------------------------------------------- \n"  | tee -a "$LOGFILE"
	# sleep 2
	echo -e -n "${nocolor}"
	sudo apt-get update && sudo apt-get upgrade -y && apt-get full-upgrade -y
}


function additional_packages() {
	# install my favorite and commonly used packages
	echo -e -n "${yellow}"
	echo -e "--------------------------------------------------- " | tee -a "$LOGFILE"
	echo -e " $(date +%m.%d.%Y_%H:%M:%S) : INSTALLING ADDITIONAL PACKAGES " | tee -a "$LOGFILE"
	echo -e "--------------------------------------------------- " | tee -a "$LOGFILE"
	echo -e -n "${nocolor}"
		apt-get -qqy -o=Dpkg::Use-Pty=0 -o=Acquire::ForceIPv4=true install \
		htop nethogs ufw fail2ban glances ntp lsb-release \
		update-motd unattended-upgrades secure-delete net-tools dnsutils | tee -a "$LOGFILE"
}


function unattended_upgrades() {
	echo -e -n "${yellow}"
	echo -e "--------------------------------------------------- " | tee -a "$LOGFILE"
	echo -e " $(date +%m.%d.%Y_%H:%M:%S) : CONFIGURE UNATTENDED UPGRADES " | tee -a "$LOGFILE"
	echo -e "--------------------------------------------------- " | tee -a "$LOGFILE"
	echo -e -n "${nocolor}"
	cat > /etc/apt/apt.conf.d/50unattended-upgrades <<EOL
Unattended-Upgrade::Allowed-Origins {
        "${distro_id}:${distro_codename}";
        "${distro_id}:${distro_codename}-security";
        // Extended Security Maintenance; doesn't necessarily exist for
        // every release and this system may not have it installed, but if
        // available, the policy for updates is such that unattended-upgrades
        // should also install from here by default.
        "${distro_id}ESMApps:${distro_codename}-apps-security";
        "${distro_id}ESM:${distro_codename}-infra-security";
//      "${distro_id}:${distro_codename}-updates";
//      "${distro_id}:${distro_codename}-proposed";
//      "${distro_id}:${distro_codename}-backports";
};
EOL
}


function sysctl_conf() {
	echo -e -n "${yellow}"
	echo -e "--------------------------------------------------- " | tee -a "$LOGFILE"
	echo -e " $(date +%m.%d.%Y_%H:%M:%S) : CONFIGURE SYSCTL.CONF " | tee -a "$LOGFILE"
	echo -e "--------------------------------------------------- " | tee -a "$LOGFILE"
	echo -e -n "${nocolor}"
	cp /etc/sysctl.conf /etc/sysctl.conf_BACKUP
	cp lib/sysctl.conf /etc/sysctl.conf
}


function sshd_conf() {
	echo -e -n "${yellow}"
	echo -e "--------------------------------------------------- " | tee -a "$LOGFILE"
	echo -e " $(date +%m.%d.%Y_%H:%M:%S) : CONFIGURE SSH " | tee -a "$LOGFILE"
	echo -e "--------------------------------------------------- " | tee -a "$LOGFILE"
	echo -e -n "${nocolor}"
	cp /etc/ssh/sshd_config /etc/ssh/sshd_config_BACKUP
	cp lib/sshd_config /etc/ssh/sshd_config
}


function update_nsntp() {
	echo -e -n "${yellow}"
	echo -e "--------------------------------------------------- " | tee -a "$LOGFILE"
	echo -e " $(date +%m.%d.%Y_%H:%M:%S) : UPDATE NAMESERVER & NTP " | tee -a "$LOGFILE"
	echo -e "--------------------------------------------------- " | tee -a "$LOGFILE"
	echo -e -n "${nocolor}"
	truncate -s0 /etc/resolv.conf
	echo "nameserver 1.1.1.1" | sudo tee -a /etc/resolv.conf
	echo "nameserver 1.0.0.1" | sudo tee -a /etc/resolv.conf
	truncate -s0 /etc/systemd/timesyncd.conf
	echo "[Time]" | sudo tee -a /etc/systemd/timesyncd.conf
	echo "NTP=time.cloudflare.com" | sudo tee -a /etc/systemd/timesyncd.conf
	echo "FallbackNTP=ntp.ubuntu.com" | sudo tee -a /etc/systemd/timesyncd.conf
}

function configure_ufw() {
	echo -e -n "${yellow}"
	echo -e "--------------------------------------------------- " | tee -a "$LOGFILE"
	echo -e " $(date +%m.%d.%Y_%H:%M:%S) : CONFIGURE UFW " | tee -a "$LOGFILE"
	echo -e "--------------------------------------------------- " | tee -a "$LOGFILE"
	echo -e -n "${nocolor}"
	ufw disable
	echo "y" | sudo ufw reset
	ufw default deny incoming
	ufw default allow outgoing
	ufw allow 80/tcp comment "http"
	ufw allow 443/tcp comment "https"
	ufw allow 941/tcp comment "ssh"
	ufw allow ssh
	sed -i "/ipv6=/Id" /etc/default/ufw
	echo "IPV6=no" | sudo tee -a /etc/default/ufw
	sed -i "/GRUB_CMDLINE_LINUX_DEFAULT=/Id" /etc/default/grub
	echo "GRUB_CMDLINE_LINUX_DEFAULT=\"ipv6.disable=1 quiet splash\"" | sudo tee -a /etc/default/grub
}


function configure_fail2ban() {
	echo -e -n "${yellow}"
	echo -e "--------------------------------------------------- " | tee -a "$LOGFILE"
	echo -e " $(date +%m.%d.%Y_%H:%M:%S) : CONFIGURE FAIL2BAN " | tee -a "$LOGFILE"
	echo -e "--------------------------------------------------- " | tee -a "$LOGFILE"
	echo -e -n "${nocolor}"
	cp lib/fail2ban /etc/fail2ban/jail.local
}

function post_configuration() {
	echo -e -n "${yellow}"
	echo -e "--------------------------------------------------- " | tee -a "$LOGFILE"
	echo -e " $(date +%m.%d.%Y_%H:%M:%S) : POST CONFIGURATION " | tee -a "$LOGFILE"
	echo -e "--------------------------------------------------- " | tee -a "$LOGFILE"
	echo -e -n "${nocolor}"
		apt-get autoremove -y
		apt-get autoclean -y
		sysctl -p
		update-grub2
		unmask systemd-timesyncd.service
		systemctl restart systemd-timesyncd
		ufw --force enable
		service ssh restart
}



create_swap
update_system
unattended_upgrades
additional_packages
sysctl_conf
sshd_conf
update_nsntp
configure_ufw
configure_fail2ban
post_configuration


# exit
exit 1

